#!/usr/bin/env node

const fs = require("fs");
const path = require("path");

const imageDir = path.join(__dirname, "..", "extension", "images");

let index = 1;
fs.readdirSync(imageDir)
  .filter((file) => file.endsWith(".jpg"))
  .forEach((file) => {
    console.log(`Renaming '${file}' to 'background-${index}.jpg'`);

    fs.renameSync(
      path.join(imageDir, file),
      path.join(imageDir, `background-${index}.jpg`)
    );

    index++;
  });
