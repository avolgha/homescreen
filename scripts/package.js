#!/usr/bin/env node
const { inlineScriptTags } = require("inline-scripts");
const inlineStylesheets = require("inline-scripts/src/inlineStylesheets");

const fs = require("fs");
const path = require("path");

function writeToFile(file, data) {
  if (fs.existsSync(file)) {
    fs.rmSync(file);
  }

  fs.writeFileSync(file, data, { encoding: "utf-8" });
}

(async () => {
  console.log("Start packaging file...");

  if (!fs.existsSync("out")) {
    fs.mkdirSync("out");
  }

  if (!fs.statSync("out").isDirectory()) {
    console.error('Please delete file "out" before running this script again!');
    process.exit(1);
  }

  console.log("Inlining script tags...");

  const firstInline = await inlineScriptTags(
    path.join(__dirname, "..", "index.html")
  );
  writeToFile(path.join(__dirname, "..", "temp.html"), firstInline);

  console.log("Inlining stylesheets...");

  const secondInline = await inlineStylesheets(
    path.join(__dirname, "..", "temp.html")
  );
  writeToFile(path.join(__dirname, "..", "out", "temp.html"), secondInline);

  console.log("Uglifying HTML file...");

  function uglify(data) {
    return data.replace(/\s+/g, " ").replace(/>\s+</g, "><");
  }

  writeToFile(
    path.join(__dirname, "..", "out", "index.html"),
    uglify(
      fs.readFileSync(path.join(__dirname, "..", "out", "temp.html"), {
        encoding: "utf-8",
      })
    )
  );

  console.log("Removing temp files...");

  fs.rmSync(path.join(__dirname, "..", "temp.html"));
  fs.rmSync(path.join(__dirname, "..", "out", "temp.html"));

  console.log(
    `Finished! You can see the output in "${path.join(
      __dirname,
      "out",
      "index.html"
    )}"`
  );
})();
