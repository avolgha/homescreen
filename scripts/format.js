#!/usr/bin/env node

const fs = require("fs");
const path = require("path");

const replacements = {
  background_images_length: () =>
    fs
      .readdirSync(path.join(__dirname, "..", "extension", "images"))
      .filter((file) => file.startsWith("background-") && file.endsWith(".jpg"))
      .length.toString(),
  git_repository: () => require("../package.json").repository.url,
};

const file = path.join(__dirname, "..", "main.mjs");
let content = fs.readFileSync(file, { encoding: "utf-8" });

for (const key in replacements) {
  console.log(`Formatting key '${key}'...`);
  content = content.replaceAll(
    `<%=${key.toUpperCase()}=%>`,
    replacements[key]()
  );
}

fs.writeFileSync(file, content, { encoding: "utf-8" });
