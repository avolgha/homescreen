#!/usr/bin/env node
const fs = require("fs");
const path = require("path");

const files = ["index.html", "iosevka-regular.ttf", "main.css", "main.mjs"];

const makePath = (file) => path.join(__dirname, "..", "extension", file);

const deleteIfExists = (file) => {
  if (fs.existsSync(makePath(file))) {
    fs.rmSync(makePath(file));
  }
};

const replace = (file) => {
  console.log(`Replacing "${file}"...`);
  deleteIfExists(file);
  fs.copyFileSync(makePath(path.join("..", file)), makePath(file));
};

console.log("Packaging Chrome extension...");

files.forEach(replace);

console.log('Finished. Output is in "extension/".');
