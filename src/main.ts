import * as Clock from "./clock";
import { BODY, SEARCH, GIT_BUTTON, IMAGE_URLS, LINKS } from "./constants";
import { getRandomItem, redirect } from "./util";

// Injects the clock code to the website
Clock.injectClock();

BODY.onload = () => {
  // set a random background image to the body
  BODY.style.backgroundImage = `url(images/${getRandomItem(IMAGE_URLS)})`;

  // add all links to the link list
  const links = document.querySelector("#links ul");
  LINKS.forEach((link) => {
    const element = document.createElement("li");
    const linkElement = document.createElement("a");
    linkElement.href = link.url;
    linkElement.innerText = link.title;
    element.appendChild(linkElement);
    links?.appendChild(element);
  });
};

// event for handle the search box. It will relovate the browser
// to the duckduckgo search site
SEARCH.onsubmit = (event) => {
  event.preventDefault();

  const content = (SEARCH.children[0] as HTMLInputElement).value;

  redirect(`https://duckduckgo.com/?q=${content}`);
};

// redirect on click to the repository
GIT_BUTTON.onclick = (event) => {
  event.preventDefault();

  redirect("<%=GIT_REPOSITORY=%>");
};

const actions: {
  key: string;
  alt: boolean | undefined;
  action: () => void;
}[] = LINKS.map((link) => {
  return {
    key: link.keys.key,
    alt: link.keys.alt,
    action: link.action,
  };
});
BODY.onkeydown = (event) => {
  actions
    .filter((action) => action.key === event.key)
    .filter((action) => {
      if (action.alt === undefined) return true;

      if (action.alt === true) {
        return event.altKey;
      } else {
        return !event.altKey;
      }
    })
    .forEach((action) => action.action());
};
