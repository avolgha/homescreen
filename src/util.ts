/**
 * Returns a random item from the given array.
 *
 * Function will throw an error if the array is empty.
 *
 * @param array The array you want to get a random item from
 * @returns The random item out of the array
 */
export function getRandomItem<T>(array: T[]): T {
  if (array.length === 0) {
    throw new Error("Cannot get a random item out of an empty array.");
  }

  return array[Math.floor(Math.random() * array.length)];
}

/**
 * Redirect to an other page
 * 
 * @param url The url you want to change to
 */
export function redirect(url: string) {
  //@ts-ignore
  window.location = url;
}
