/**
 * Inject the code for clocks into the each element with the class `clock`
 *
 * It will render a clock in it with hours, minutes and seconds that will 
 * update every second through a loop
 */
export function injectClock() {
  /**
   * Format the current date to something like this:
   * 
   * ```
   * 12:45:43
   * ```
   * 
   * @param date The date you want to format. Default to current date
   * @returns The formatted date as string
   */
  function formatDate(date: Date = new Date()): string {
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();

    // this will put a zero in front of each if the value is less than 10
    return `${hours > 9 ? hours : "0" + hours}:${
      minutes > 9 ? minutes : "0" + minutes
    }:${seconds > 9 ? seconds : "0" + seconds}`;
  }

  const element = document.querySelector(".clock");
  //@ts-ignore
  element.textContent = formatDate();

  // replace the content each second
  setInterval(() => {
    if (!element) return;

    element.textContent = formatDate();
  }, 1000);
}
