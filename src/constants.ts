import { redirect } from "./util";

/**
 * An array with urls that could be displayed as background images
 *
 * See `main.ts` for more information
 */
export const IMAGE_URLS: string[] = new Array(
  parseInt("<%=BACKGROUND_IMAGES_LENGTH=%>")
)
  .fill("")
  .map((_, index) => `background-${index + 1}.jpg`);

/**
 * The links on the website
 */
export const LINKS: {
  title: string;
  url: string;
  keys: {
    key: string;
    alt: boolean | undefined;
  };
  action: () => void;
}[] = [
  {
    title: "GitLab",
    url: "https://gitlab.com/avolgha/",
    keys: {
      key: "1",
      alt: true,
    },
    action: () => redirect("https://gitlab.com/avolgha/"),
  },
  {
    title: "GitHub",
    url: "https://github.com/avolgha/",
    keys: {
      key: "2",
      alt: true,
    },
    action: () => redirect("https://github.com/avolgha/"),
  },
  {
    title: "DuckDuckGo",
    url: "https://duckduckgo.com/",
    keys: {
      key: "3",
      alt: true,
    },
    action: () => redirect("https://duckduckgo.com/"),
  },
  {
    title: "StartPage",
    url: "https://startpage.com/",
    keys: {
      key: "4",
      alt: true,
    },
    action: () => redirect("https://startpage.com/"),
  },
  {
    title: "GMail",
    url: "https://gmail.google.com/",
    keys: {
      key: "5",
      alt: true,
    },
    action: () => redirect("https://gmail.google.com/"),
  },
  {
    title: "Maps",
    url: "https://maps.google.com/",
    keys: {
      key: "6",
      alt: true,
    },
    action: () => redirect("https://maps.google.com/"),
  },
  {
    title: "Markdown Badges",
    url: "https://github.com/Ileriayo/markdown-badges/",
    keys: {
      key: "7",
      alt: true,
    },
    action: () => redirect("https://github.com/Ileriayo/markdown-badges/"),
  },
];

/**
 * The `body` element of the website
 */
export const BODY: HTMLBodyElement = document.querySelector(
  "body"
) as any as HTMLBodyElement;

/**
 * The search form if the website
 */
export const SEARCH: HTMLFormElement = document.querySelector(
  "#search form"
) as any as HTMLFormElement;

/**
 * The git action button to come to the repository
 */
export const GIT_BUTTON: HTMLButtonElement = document.querySelector(
  "#git-icon button"
) as any as HTMLButtonElement;
