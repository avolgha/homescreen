<h1 align="center">Home Screen</h1>

<p align="center">
<img src="https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white" alt="TypeScrpt" />
<img src="https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white" alt="CSS3" />
<img src="https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white" alt="HTML5" />
</p>

<h3 align="center">A custom made home screen for myself</h3>

## » Table of Contents

* [Structure](#-structure)
* [Bundle and Minify](#-bundle-and-minify)
* [Extension](#-extension)
* [Customization](#-customization)
* [NodeJS Scripts](#-scripts)
* [Dependencies](#-dependencies)
* [Support](#-support)

## » Structure

```bash
- css/                    # This folder contains all additional style files
  - *.scss
- extension               # Contents for the Chrome Extension
  - images/               # Images used by the extension
    - *.[jpg,png...]
  - manifest.json         # Manifest file for declaring the extension
- out/                    # Contains the bundled index.html file
  - index.html
- scripts/
  - format.js             # Format the minified JavaScript file with custom strings
  - package.js            # Script to bundle and minify the `index.html` file
  - packExtension.js      # Script to pack all required files into the `extension` folder
  - renameBackgrounds.js  # (! TEMP SCRIPT !) Rename all files in image folder to `background-x.jpg`
- src/                    # TypeScript source files that will be compiled to JavaScript to run in browser
  - clock.ts              # Code for the Clock
  - constants.ts          # Constants used in the later process
  - main.ts               # Main file from where all other things should be called
  - util.ts               # Utility things
- index.html              # Original main html file without bundling
- iosevka-regular.ttf     # Iosevka Font for use in short link list
- main.css                # Output of compiled SCSS
- main.mjs                # Output of compiled TypeScript
- main.scss               # Root Style file for the project
- package.json            # Declaration file for the NodeJS project
- README.md               # Description file of the project
- tsconfig.json           # TypeScript configuration file
- yarn.lock               # yarnpkg store file
```

## » Bundle and Minify

If you want to self compile this page, you need to have [yarn](https://yarnpkg.com) installed or you have to rewrite the scripts in the `package.json` for your need to `npm` or `pnpm`

To bundle all the sources into one file, you need to build all the sources and styles.

To bundle and minify all files into one file, run

```bash
$ yarn package
```

This will output the bundled and minified HTML file to `out/index.html`

## » Extension

You can also pack all together into a Chrome extension. To load the extension, you have to enable the `Devleloper Mode` on the Extensions-page so that you can import custom extensions.

Then you have to run

```bash
$ yarn extension
```

to create the Chrome extension

In Chrome, you have to click the button with `Load Extension` and then you will have to navigate to the `extension/` folder and click okay

Then if you open a new tab, you will be navigated to that page automatically.

## » Customization

The extension is created for myself so you have to edit the files by yourself.

The styling for the page can you customize in `main.scss`. If you want to make a multi file styling, you can use the `css/` folder too.

The scripting is all done in [TypeScript](https://www.typescriptlang.org). You could change it, but it is not recommended.

For other things like the displayed name or the links in the sidebar you will need to edit the `ìndex.html` file.

Also there are background images that you can customize. For this, you have to edit `IMAGE_URLS` in `src/constants.ts`. This should be one of these formats: [JPEG](https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types#jpeg_joint_photographic_experts_group_image), [GIF](https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types#gif_graphics_interchange_format), [PNG](https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types#png_portable_network_graphics), [APNG](https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types#apng_animated_portable_network_graphics), [SVG](https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types#svg_scalable_vector_graphics), [AVIF](https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types#avif_image) and [WEBP](https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types#webp_image)

> The supported formats are copied from [mozilla.org docs](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img#supported_image_formats) and could change in the time between I wrote this README and you read this

## » Scripts

There are a few scripts declared in the `package.json` file. You can find a list of them here:

Name|Command|Description
---|---|---
watch:js|`yarn watch:js`|Watch for changes in the `src/` directory and compile all files in it (see `build:js`)
watch:css|`yarn watch:css`|Watch for changes in the `main.scss` file and compile it (see `build:css`)
cleanup|`yarn cleanup`|Remove files that can be recreated from the project except from the NodeJS modules
build:all|`yarn build:all`|Run `build:js` and `build:css`
build:js|`yarn build:js`|Build all sources in the `src/` directory to `main.mjs`
build:css|`yarn build:css`|Compile `main.scss` file to `main.css`
package|`yarn package`|Minify all required sources (except the images) to `out/`. This **will** throw an error if you use images that are stored localy
extension|`yarn extension`|Pack all required sources to a Chrome extension in `extension/`. For more information here, see [this](#-extension)

## » Dependencies

### `inline-scripts` (^1.7.4)

`inline-scripts` is used to bundle the stylesheets and JavaScript files into the HTML file

### `nodemon` (^2.0.15)

`nodemon` is used to create a process to compile the TypeScript source files

### `rimraf` (^3.0.2)

`rimraf` is used to delete files and directories on cleanup

### `sass` (^1.45.1)

`sass` is used to compile the SCSS files to plain CSS

### `tsup` (^5.11.8)

`tsup` is used to compile, bundle and minify the TypeScript files to browser-usable JavaScript in ESNext format

### `typescript` (^4.4.2)

`typescript` is used as library for the TypeScript source files

## » Support

To get support, just create a ticket/issue on this repository.

You can optionally write me on my discord `Marius#0686`
